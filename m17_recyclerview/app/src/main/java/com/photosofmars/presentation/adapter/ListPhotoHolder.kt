package com.photosofmars.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.photosofmars.databinding.FragmentItemListPhotoBinding

class ListPhotoHolder (val binding: FragmentItemListPhotoBinding): RecyclerView.ViewHolder(binding.root)