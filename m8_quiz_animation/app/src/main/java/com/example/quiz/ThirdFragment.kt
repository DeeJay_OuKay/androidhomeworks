package com.example.quiz

import android.animation.AnimatorInflater
import android.animation.ObjectAnimator
import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.quiz.databinding.FragmentThirdBinding


class ThirdFragment : Fragment() {

    private var _binding: FragmentThirdBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentThirdBinding.inflate(inflater, container, false)

//        binding.result.animate().apply {
//            duration = 2000
//            //rotation(360f)
//            //translationX(50f)
//            translationY(10f)
//            //translationZ(-10f)
//            scaleX(.9f)
//            scaleY(.9f)
//            interpolator = AccelerateInterpolator()
//        }.start()

//        binding.button.animate().apply {
//            duration = 3000
//            //rotation(360f)
//            //translationX(50f)
//            translationY(-200f)
//            translationZ(10f)
//            scaleX(1.5f)
//            scaleY(1.5f)
//            interpolator = AccelerateDecelerateInterpolator()
//        }.start()

        ObjectAnimator.ofFloat(
            binding.result,
            View.ROTATION,
            0f,
            720f
        ).apply{
            duration = 4000
            interpolator = AccelerateDecelerateInterpolator()
        }.start()

        //в context вместо this нужен activity, так как работаем не с активити, а с фрагментом
        val activity: Activity? = activity
        (AnimatorInflater.loadAnimator(activity,R.animator.result_animation) as ObjectAnimator).apply {
            target = binding.button
            start()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.result.text = arguments?.getString("result")
        val allCorrect = arguments?.getBoolean("allCorrect")
        val color = if (allCorrect == true)
            "#00FF00"
        else
            "#FF0000"

        ObjectAnimator.ofArgb(
            binding.button,
            "textColor",
            Color.parseColor("#FFFFFF"),
            Color.parseColor(color),
        ).apply{
            duration = 1000
            interpolator = AccelerateInterpolator()
            repeatCount = ObjectAnimator.INFINITE
            repeatMode = ObjectAnimator.REVERSE
        }.start()

        binding.button.setOnClickListener {
            findNavController().navigate(R.id.action_ThirdFragment_to_SecondFragment)
        }
        Toast.makeText(requireContext(), "Цвет текста кнопки зависит от результата", Toast.LENGTH_SHORT).show();
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}