package com.attractions

import android.app.Application
import com.yandex.mapkit.MapKitFactory
import dagger.hilt.android.HiltAndroidApp

private const val API_KEY = "718ad9d1-e794-45f6-a0e3-53190856cd6e"

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        MapKitFactory.setApiKey(API_KEY)
    }
}