package com.example.m14_retrofit.model

data class Id(
    val name: String,
    val value: String?
)