package com.example.m16_architecture.data

import com.example.m16_architecture.entity.UsefulActivity
import javax.inject.Inject

class UsefulActivitiesRepository @Inject constructor(private val api: DataSource.SearchSource ) {

    suspend fun getUsefulActivity(): UsefulActivity {
        return api.getApi()
    }
}