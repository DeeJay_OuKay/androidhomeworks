package com.example.m16_architecture.di

import com.example.m16_architecture.data.DataSource
import com.example.m16_architecture.data.DataSource.SearchSource
import com.example.m16_architecture.data.UsefulActivitiesRepository
import dagger.Module
import dagger.Provides

@Module
class DataModule {
    @Provides
    fun provideUsefulActivitiesRepository(api: SearchSource): UsefulActivitiesRepository {
        return UsefulActivitiesRepository(api)
    }

    @Provides
    fun provideApi(): SearchSource {
        return DataSource.RetrofitService.searchImageApi
    }
}