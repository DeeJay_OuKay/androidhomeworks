package com.example.m14_retrofit.model

data class Street(
    val number: Int,
    val name: String
)
