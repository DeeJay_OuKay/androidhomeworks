package com.example.m16_architecture.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.m16_architecture.domain.GetUsefulActivityUseCase
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainViewModel(
    private val getUsefulActivityUseCase: GetUsefulActivityUseCase
) : ViewModel() {
    private val _state =
        MutableStateFlow<State>(State.Success("Нажать кнопку \"Обновить\""))
    val state = _state.asStateFlow()
    private val _error = Channel<String>()

    fun reloadUsefulActivity() {
        viewModelScope.launch {
            try {
                _state.value = State.Loading
                val result = getUsefulActivityUseCase.execute().activity
                _state.value = State.Success(result)
            } catch (e: Exception) {
                _state.value = State.Error(e)
                _error.send(e.toString())
            }
        }
    }
}

