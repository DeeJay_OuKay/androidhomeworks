package com.example.m11_timer_data_storage

import android.content.Context
import android.content.SharedPreferences

private const val PREFERENCE_NAME = "prefs_name"
private const val KEY_STRING_NAME = "key_string"
class Repository (context: Context) {
    private var prefs = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor = prefs.edit()
    private var localVar: String? = null
    // будет доставать значение из SharedPreference;
    private fun getDataFromSharedPreference() = prefs.getString(KEY_STRING_NAME, null)
    // будет доставать значение, возвращать значение локальной переменной;
    private fun getDataFromLocalVariable() = localVar
    // будет записывать значения и в SharedPreference, и в локальную переменную.
    fun saveText(text: String) {
        editor.putString(KEY_STRING_NAME,text).apply()
    }
    // будет очищать значение и в SharedPreference, и в локальной переменной.
    fun clearText() {
        editor.remove(KEY_STRING_NAME).apply()
    }
    // будет доставать значение из источников: сначала попытается взять значение локальной переменной; если оно null, то попытаемся взять значение из SharedPreference.
    fun getText(): String {
        return when {
            getDataFromLocalVariable() != null -> getDataFromLocalVariable()!!
            getDataFromSharedPreference() != null -> getDataFromSharedPreference()!!
            else -> ""
        }
    }
}