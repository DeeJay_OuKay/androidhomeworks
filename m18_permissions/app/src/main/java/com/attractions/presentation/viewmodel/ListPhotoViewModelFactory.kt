package com.attractions.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class ListPhotoViewModelFactory @Inject constructor(
    private val listPhotoViewModel: ListPhotoViewModel
): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListPhotoViewModel::class.java)) {
            return listPhotoViewModel as T
        }
        throw IllegalArgumentException("Неизвестное имя класса")
    }
}