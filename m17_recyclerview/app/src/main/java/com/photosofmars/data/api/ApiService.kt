package com.photosofmars.data.api

import com.photosofmars.data.dto.PhotosDto
import retrofit2.http.GET
import retrofit2.http.Query

private const val API_KEY = "4zKyUUNHssfeCNCG0CeH9SffZLOjq1mWBLx03Sit"

interface ApiService {
    @GET("/mars-photos/api/v1/rovers/curiosity/photos")
    suspend fun getPhotos(
        @Query("earth_date") earthDate: String,
        @Query("api_key") apiKey: String = API_KEY
    ): PhotosDto
}