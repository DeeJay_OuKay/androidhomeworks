package com.attractions.data

import androidx.room.TypeConverter
import java.util.Calendar

class Converters {
    @TypeConverter
    fun fromCalendar(calendar: Calendar?): Long? = calendar?.time?.time

    @TypeConverter
    fun toCalendar(millis: Long?): Calendar? =
        if (millis == null) null else Calendar.getInstance().apply { timeInMillis = millis }
}