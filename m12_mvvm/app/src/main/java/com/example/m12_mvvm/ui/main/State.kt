package com.example.m12_mvvm.ui.main

sealed class State {
    object Start: State()
    object Loading: State()
    data class Success(val request: String): State()
}