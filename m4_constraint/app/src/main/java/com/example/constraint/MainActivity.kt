package com.example.constraint

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.widget.doOnTextChanged
import com.example.constraint.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.nameEdit.doOnTextChanged {text, _, _, _ ->
            if (text.isNullOrBlank()) {
                binding.name.error = getText(R.string.name_error)
                binding.name.isErrorEnabled = true
            }
            else
                binding.name.isErrorEnabled = false
            validation()
        }
        binding.phoneEdit.doOnTextChanged {text, _, _, _ ->
            if (text.isNullOrBlank()) {
                binding.phone.error = getText(R.string.phone_error)
                binding.phone.isErrorEnabled = true
            }
            else
                binding.phone.isErrorEnabled = false
            validation()
        }

        binding.notifications.setOnClickListener {notificationsToggle()}
        binding.male.setOnClickListener {validation()}
        binding.female.setOnClickListener {validation()}
        binding.auth.setOnClickListener {validation()}
        binding.news.setOnClickListener {validation()}

        binding.score.progress = Random.nextInt(binding.score.max+1)
        binding.scoreValue.text = buildString {
            append(binding.score.progress.toString())
            append(getText(R.string.scores_end))
        }
        binding.save.setOnClickListener {save()}
        validation()
    }

    private fun notificationsToggle() {
        if (binding.notifications.isChecked) {
            binding.auth.isEnabled = true
            binding.news.isEnabled = true
        } else {
            binding.auth.isEnabled = false
            binding.news.isEnabled = false
        }
        validation()
    }

    private fun validation() {
        binding.save.isEnabled = nameIsCorrect() && phoneIsCorrect() && sexIsCorrect() && notificationsAreCorrect()
    }

    private fun save() {
        showMessage(getText(R.string.save_ok).toString())
    }

    private fun nameIsCorrect(): Boolean {
        return !binding.nameEdit.text.isNullOrEmpty()
    }

    private fun phoneIsCorrect(): Boolean {
        return !binding.phoneEdit.text.isNullOrEmpty()
    }

    private fun sexIsCorrect(): Boolean {
        return binding.male.isChecked || binding.female.isChecked
    }

    private fun notificationsAreCorrect(): Boolean {
        return !binding.notifications.isChecked || (binding.notifications.isChecked && (binding.auth.isChecked || binding.news.isChecked))
    }

    private fun showMessage(message: String) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).show()
    }
}