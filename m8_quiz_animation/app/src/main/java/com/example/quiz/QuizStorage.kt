package com.example.quiz

object QuizStorage {
    fun getQuiz(): List<Poll> {
        return listOf(
            Poll(
                question = "Я - сама неотвратимость",
                author = "Танос",
                answers = listOf(
                    "А я - Железный Человек",
                    "Не выражаться",
                    "Я - Тор, сын Одина, и покуда в груди моей бьётся сердце, я... не мастер долгих и занудных речей"
                ),
                hints = listOf(
                    "Тони Старк",
                    "Стив Роджерс",
                    "Тор Одинсон"
                ),
                correct = 0
            ),
            Poll(
                question = "Где ты его взял?",
                author = "Какой-то браконьер",
                answers = listOf(
                    "Целую жизнь назад я тоже искал камни",
                    "Забавно, что ты спросил",
                    "Это не ты нашел нас, это мы нашли тебя"
                ),
                hints = listOf(
                    "Красный Череп",
                    "Росомаха",
                    "Веном"
                ),
                correct = 1
            ),
            Poll(
                question = "А какая у тебя супер сила?",
                author = "Флэш",
                answers = listOf(
                    "Знаешь, я тоже, в общем-то, учёный",
                    "Глаза, лёгкие, желудок... Так бы и съел, да времени в обрез",
                    "Деньги"
                ),
                hints = listOf(
                    "Норман Осборн",
                    "Веном",
                    "Бэтмен"
                ),
                correct = 2
            )
        )
    }

    fun answer(answers: List<String?>): Pair<String,Boolean>{
        /*  Цитата из ДЗ:
            "обрабатываются результаты опросника с помощью функции QuizStorage.answer().
            Полученный результат нужно отправить в третий экран «Результаты» в качестве аргумента."
         */
        val polls = getQuiz()
        var result = ""
        var allCorrect = true
        for ((i, poll) in polls.withIndex())
        {
            if (answers[i]?.equals(poll.answers[poll.correct]) == true)
                result += "" +
                        "Верный ответ:\n${poll.author}: \"${poll.question}\"\n" +
                        "${poll.hints[poll.correct]}: \"${poll.answers[poll.correct]}\"\n"
            else
            {
                allCorrect = false
                result += "" +
                        "НЕВЕРНЫЙ ответ:\n\"${poll.question}\"\n" +
                        "Вопрос задал ${poll.author}, а ответить должен: ${poll.hints[poll.correct]}\n"
                for ((j, answer) in poll.answers.withIndex())
                {
                    if (answer == answers[j])
                        result += "Ваш ответ кому-то другому \"${answer}\" дал ${poll.hints[j]}"
                }
            }
            if (i < polls.size)
                result += "\n"
        }
        return Pair(result,allCorrect)
    }
}
