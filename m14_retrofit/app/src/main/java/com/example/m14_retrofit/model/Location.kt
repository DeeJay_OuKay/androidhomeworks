package com.example.m14_retrofit.model

data class Location(
    val street: Street,
    val city: String,
    val state: String,
    val country: String
)
