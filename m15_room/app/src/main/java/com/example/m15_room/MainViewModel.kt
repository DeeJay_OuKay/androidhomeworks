package com.example.m15_room

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class MainViewModel(private val wordDao: WordDao): ViewModel() {
    val allWords = this.wordDao.getAll()
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000L),
            initialValue = emptyList()
        )
    fun onAddButton(newWord: String): String{
        for (word in allWords.value)
            if (word.id == newWord)
            {
                plusOne(newWord)
                return "Кол-во повторений слова увеличено на 1"
            }
        if (isValid(newWord))
        {
            viewModelScope.launch{
                wordDao.insert(
                    Word(
                        id = newWord
                    )
                )
            }
            return "Новое слово добавлено"
        }
        else
            return "Некорректное слово"
    }

    private fun isValid(word: String): Boolean {
        return (word.isNotBlank() && word.matches(Regex("[a-zA-Zа-яА-Я-']+")))
    }
    private fun plusOne(oldWord: String){
        viewModelScope.launch {
            allWords.value.lastOrNull()?.let {
                wordDao.update(oldWord)
            }
        }
    }
    fun onDeleteBtn(){
        viewModelScope.launch {
            wordDao.delete()
        }
    }
}