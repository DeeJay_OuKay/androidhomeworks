package com.example.m14_retrofit.exception

class ApiException(message: String): Exception(message)