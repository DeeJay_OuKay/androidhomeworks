package com.example.m14_retrofit.model

data class Picture(
    val large: String?
)