package com.example.m14_retrofit.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.m14_retrofit.R
import com.example.m14_retrofit.repository.UserRepository
import com.example.m14_retrofit.ui.viewmodel.UserViewModel

class UserViewModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserViewModel::class.java)) {
            return UserViewModel(UserRepository()) as T
        }
        throw IllegalArgumentException(R.string.unknown_class.toString())
    }
}