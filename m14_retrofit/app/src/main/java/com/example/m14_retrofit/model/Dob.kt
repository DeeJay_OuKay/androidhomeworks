package com.example.m14_retrofit.model

data class Dob(
    val age: Int
)
