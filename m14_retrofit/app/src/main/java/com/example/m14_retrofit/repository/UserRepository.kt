package com.example.m14_retrofit.repository

import com.example.m14_retrofit.R
import com.example.m14_retrofit.api.RetroBuilder
import com.example.m14_retrofit.exception.ApiException
import com.example.m14_retrofit.model.Results

class UserRepository() {
    suspend fun getRandomUser(): Results? {
        val response = RetroBuilder.api.getRandomUser()
        if(!response.isSuccessful)
            throw ApiException(R.string.request_failed.toString())
        return response.body()
    }
}