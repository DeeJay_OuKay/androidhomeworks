package com.example.m14_retrofit.model

data class Name(
    val first: String,
    val last: String
)
