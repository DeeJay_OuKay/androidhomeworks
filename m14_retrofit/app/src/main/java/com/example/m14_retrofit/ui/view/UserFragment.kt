package com.example.m14_retrofit.ui.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.m14_retrofit.databinding.FragmentUserBinding
import com.example.m14_retrofit.ui.base.UserViewModelFactory
import com.example.m14_retrofit.ui.viewmodel.State
import com.example.m14_retrofit.ui.viewmodel.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch
import androidx.fragment.app.viewModels
import com.example.m14_retrofit.R

class UserFragment : Fragment() {
    private var _binding: FragmentUserBinding? = null
    private val binding get() = _binding!!
    private val viewModel: UserViewModel by viewModels { UserViewModelFactory() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUserBinding.inflate(layoutInflater)
        return binding.root
    }

    @SuppressLint("StringFormatMatches")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null)
            viewModel.getUserData()

        binding.apply {
            button.setOnClickListener {
                viewModel.getUserData()
            }

            viewLifecycleOwner.lifecycleScope.launch {
                viewModel.state.collect {
                    when(it) {
                        State.Loading -> {
                            progress.isVisible = true
                            button.isEnabled = false
                        }
                        is State.Error -> {
                            progress.isVisible = false
                            button.isEnabled = true
                        }
                        State.Finish -> {
                            progress.isVisible = false
                            button.isEnabled = true
                        }
                    }
                }
            }

            viewLifecycleOwner.lifecycleScope.launch {
                viewModel.resultsFlow.collect {
                    it?.results?.first()?.apply {
                        data.text = getString(
                            R.string.user_data,
                            login.username,name.first,name.last,dob.age,
                            email,phone,cell,
                            location.country,location.state,location.street.name,location.street.number
                        ).trimIndent()
                        Glide.with(view)
                            .load(picture.large)
                            .into(image)
                    }
                }
            }

            viewLifecycleOwner.lifecycleScope.launch {
                viewModel.error.collect {
                    Snackbar.make(view, it, Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}