package com.example.helloworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.helloworld.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private var counter = 0
    private var min = 0
    // В автобусе 49 сидячих мест
    private var max = 49
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.plus.setOnClickListener {statusChange(1)}
        binding.minus.setOnClickListener {statusChange(-1)}
        // Кнопка «Сброс» не только сбрасывает счётчик, но и возвращает приложение в первоначальное состояние (скриншот 1).
        binding.reset.setOnClickListener {statusChange(-counter)}
        // При запуске сразу сбрасываем
        statusChange()
    }

    // Обработка текущего значения и изменение состояния программы описано в одном методе (не допускайте дублирования кода).
    private fun statusChange(plus: Int = 0) {
        val averageStatus = binding.status.context.getText(R.string.average_status)

        // Мы хотим отправлять автобусы максимально наполненными, но допускать превышения нельзя
        // Если "допускать превышение нельзя" означает, что кнопка "+" должна быть недоступна, то раскомментируем следующую строку
        // binding.plus.isEnabled = true

        // (не допускайте дублирования кода)
        // Если это не писать здесь, то ниже придётся писать этот код дважды
        binding.reset.visibility = View.INVISIBLE

        // Нажатие кнопок «+» и «-» изменяет значение счётчика и тестовых полей на «+1» и «-1» соответственно
        counter += plus

        // Задавать значение меньше 0 нельзя
        if (counter < min)
            counter = min

        binding.counter.text = counter.toString()

        // При минимальном значении состояние - по умолчанию
        if (counter == min) {
            // Если все места свободны, текст по середине — зелёный, кнопка «-» — недоступна
            binding.status.text = getText(R.string.default_status)
            binding.minus.isEnabled = false
            binding.status.setTextColor(getColorStateList(R.color.green))
        }
        else if ((counter > min) && (counter <= max)) {
            // При количестве пассажиров от 1 до 49 текст синий, кнопки доступны. Кнопка «Сброс» невидимая
            "$averageStatus ${max - counter}".also { binding.status.text = it }
            binding.status.setTextColor(getColorStateList(R.color.blue))
            binding.minus.isEnabled = true
        }
        else {
            // При количестве пассажиров 50 и выше текст красный, появляется кнопка «Сброс».
            binding.status.text = getText(R.string.warning_status)
            binding.reset.visibility = View.VISIBLE
            binding.status.setTextColor(getColorStateList(R.color.red))
            // Мы хотим отправлять автобусы максимально наполненными, но допускать превышения нельзя
            // Если "допускать превышение нельзя" означает, что кнопка "+" должна быть недоступна, то раскомментируем следующую строку
            // binding.plus.isEnabled = false
        }
    }
}