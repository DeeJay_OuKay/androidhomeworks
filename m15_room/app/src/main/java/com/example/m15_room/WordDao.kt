package com.example.m15_room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import kotlinx.coroutines.flow.Flow

@Dao
interface WordDao {
    @Transaction
    @Query("SELECT * FROM words LIMIT 5")
    fun getAll(): Flow<List<Word>>
    @Insert(entity = Word::class)
    suspend fun insert(word: Word)
    @Query("DELETE FROM words")
    suspend fun delete()
    @Query("UPDATE words SET count = count+1 WHERE id = :id")
    suspend fun update(id: String)
}