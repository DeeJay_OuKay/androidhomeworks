package com.example.m12_mvvm.ui.main

import androidx.fragment.app.viewModels
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.m12_mvvm.R
import com.example.m12_mvvm.databinding.MainFragmentBinding
import kotlinx.coroutines.launch

class MainFragment : Fragment() {
    private lateinit var binding: MainFragmentBinding
    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.query.addTextChangedListener {
            binding.button.isEnabled = !(it.isNullOrEmpty() || it.length < 3)
        }
        binding.button.setOnClickListener {
            viewModel.onFindClick(binding.query.text.toString())
        }
        viewLifecycleOwner.lifecycleScope
            .launch {
                repeatOnLifecycle(Lifecycle.State.STARTED){
                    viewModel.state
                        .collect { state ->
                            when (state) {
                                is State.Start -> {
                                    binding.query.isEnabled = true
                                    binding.button.isEnabled= false
                                    binding.searchResult.text = getString(R.string.result_default)
                                    binding.progress.isVisible = false
                                }
                                is State.Loading -> {
                                    binding.query.isEnabled = false
                                    binding.button.isEnabled= false
                                    binding.searchResult.text = getString(R.string.result_default)
                                    binding.progress.isVisible = true
                                }
                                is State.Success -> {
                                    binding.query.isEnabled = true
                                    binding.button.isEnabled = true
                                    binding.searchResult.text = getString(R.string.result_fake).replace("!query!", state.request)
                                    binding.progress.isVisible = false
                                }
                            }
                        }
                }
            }
    }
}