package com.example.quiz

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.quiz.databinding.FragmentSecondBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class SecondFragment : Fragment() {
    private var q1: String? = null
    private var q2: String? = null
    private var q3: String? = null
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)

        getPolls(binding.root)

        binding.answer11.setOnClickListener {q1 = binding.answer11.text.toString()}
        binding.answer12.setOnClickListener {q1 = binding.answer12.text.toString()}
        binding.answer13.setOnClickListener {q1 = binding.answer13.text.toString()}
        binding.answer21.setOnClickListener {q2 = binding.answer21.text.toString()}
        binding.answer22.setOnClickListener {q2 = binding.answer22.text.toString()}
        binding.answer23.setOnClickListener {q2 = binding.answer23.text.toString()}
        binding.answer31.setOnClickListener {q3 = binding.answer31.text.toString()}
        binding.answer32.setOnClickListener {q3 = binding.answer32.text.toString()}
        binding.answer33.setOnClickListener {q3 = binding.answer33.text.toString()}

        binding.question1.alpha = 0f
        binding.question1.animate().apply {
            duration = 1000
            alpha(1f)
        }.start()
        binding.question2.alpha = 0f
        binding.question2.animate().apply {
            duration = 2000
            alpha(1f)
            interpolator = AccelerateInterpolator()
        }.start()
        binding.question3.alpha = 0f
        binding.question3.animate().apply {
            duration = 3000
            alpha(1f)
            interpolator = AccelerateDecelerateInterpolator()
        }.start()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonBack.setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }
        binding.buttonSend.setOnClickListener {
            val result = QuizStorage.answer(listOf(q1,q2,q3),view)
            val bundle = bundleOf(
                "result" to result.first,
                "allCorrect" to result.second
            )
            findNavController().navigate(R.id.action_SecondFragment_to_ThirdFragment, bundle)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getPolls(view: View){
        val polls = QuizStorage.getQuiz(view)
        binding.questionText1.text = polls[0].question
        binding.questionText2.text = polls[1].question
        binding.questionText3.text = polls[2].question
        binding.answer11.text = polls[0].answers[0]
        binding.answer12.text = polls[0].answers[1]
        binding.answer13.text = polls[0].answers[2]
        binding.answer21.text = polls[1].answers[0]
        binding.answer22.text = polls[1].answers[1]
        binding.answer23.text = polls[1].answers[2]
        binding.answer31.text = polls[2].answers[0]
        binding.answer32.text = polls[2].answers[1]
        binding.answer33.text = polls[2].answers[2]
    }
}