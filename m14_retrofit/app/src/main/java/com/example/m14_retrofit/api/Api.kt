package com.example.m14_retrofit.api

import com.example.m14_retrofit.model.Results
import retrofit2.Response
import retrofit2.http.GET

interface Api {
    @GET("/api/")
    suspend fun getRandomUser() : Response<Results>
}