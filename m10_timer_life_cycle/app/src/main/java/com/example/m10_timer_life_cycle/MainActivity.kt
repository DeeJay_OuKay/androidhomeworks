package com.example.m10_timer_life_cycle

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.example.m10_timer_life_cycle.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var handler = Handler(Looper.getMainLooper())
    private var timerStartValue = 0
    private var timerCurrentValue = 0
    private var isTimerActive = false
    private lateinit var thread: Thread
    private var threadStopped = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        startPositions()

        //если есть, что восстановить
        if (savedInstanceState != null) {
            //восстанавливаем сохранённое состояние
            timerStartValue = savedInstanceState.getInt(TIMER_CURRENT_VALUE)
            isTimerActive = savedInstanceState.getBoolean(IS_TIMER_ACTIVE)
            binding.progressBar.max = timerStartValue
            timerCurrentValue = timerStartValue
            handler.post {
                timerUpdate()
                if (isTimerActive)
                    timerRun()
            }
        }

        binding.button.setOnClickListener {
            buttonClick()
            showUpdates()
        }
        binding.slider.addOnChangeListener { _, value, _ ->
            timerStartValue = value.toInt()
            timerCurrentValue = timerStartValue
            showUpdates()
        }
    }

    //нажимаем кнопку
    private fun buttonClick(){
        if (isTimerActive)
            timerStop()
        else
            timerRun()
    }

    //задаём начальные значения
    private fun startPositions() {
        timerStartValue = binding.slider.value.toInt()
        timerCurrentValue = timerStartValue
        showUpdates()
    }

    //запускаем таймер
    private fun timerRun() {
        thread = Thread {
            while (!threadStopped && (timerCurrentValue > 0)) {
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                timerCurrentValue--
                handler.post {
                    timerUpdate()
                }
            }
            handler.post {
                timerStop()
            }
        }
        isTimerActive = true
        handler.post {
            showUpdates()
        }
        thread.start()
    }

    //обновляем таймер
    private fun timerUpdate() {
        binding.timer.text = timerCurrentValue.toString()
        binding.progressBar.progress = timerCurrentValue
    }

    //останавливаем таймер
    private fun timerStop() {
        timerStartValue = binding.slider.value.toInt()
        timerCurrentValue = timerStartValue
        isTimerActive = false
        //со строкой ниже таймер не запускается после остановки
        //без этой строки таймер вместо остановки перезапускается
        //если по истчении таймера повернёть экран, глюк самоустраняется
        stopTimerThread()
        handler.post {
            showUpdates()
        }
    }

    //останавливаем поток таймера
    private fun stopTimerThread() {
        threadStopped = true
        thread.interrupt()
    }

    //отображаем обновления
    private fun showUpdates() {
        when (isTimerActive) {
            true -> {
                binding.slider.isEnabled = false
                binding.button.text = getText(R.string.button_stop)
            }
            false -> {
                binding.slider.isEnabled = true
                binding.button.text = getText(R.string.button_start)
            }
        }
        binding.progressBar.max = binding.slider.value.toInt()
        binding.progressBar.progress = timerCurrentValue
        binding.timer.text = timerStartValue.toString()
    }

    override fun onPause() {
        super.onPause()
        try {
            stopTimerThread()
        } catch (e: Exception) {
            //
        }
    }

    //сохраняем текущее состояние
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(TIMER_CURRENT_VALUE, timerCurrentValue)
        outState.putBoolean(IS_TIMER_ACTIVE, isTimerActive)
        super.onSaveInstanceState(outState)
    }

    //защищаемся от собственных косяков
    companion object {
        const val TIMER_CURRENT_VALUE = "timerCurrentValue"
        const val IS_TIMER_ACTIVE = "isTimerActive"
    }
}
