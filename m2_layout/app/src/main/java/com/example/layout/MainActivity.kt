package com.example.layout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.layout.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val posts = arrayOf(binding.post1, binding.post2, binding.post3)
        var i = 1
        for (post in posts) {
            post.setTopText("верхняя строчка $i-го блока, настроенная из кода")
            post.setBottomText("нижняя строчка $i-го блока, настроенная из кода")
            i++
        }

    }
}