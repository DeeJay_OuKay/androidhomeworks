package com.example.m14_retrofit.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.m14_retrofit.exception.ApiException
import com.example.m14_retrofit.model.Results
import com.example.m14_retrofit.repository.UserRepository
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class UserViewModel(private val repository: UserRepository)  : ViewModel() {
    private val _results: MutableStateFlow<Results?> = MutableStateFlow(null)
    var resultsFlow = _results.asStateFlow()
    private val _state = MutableStateFlow<State>(State.Loading)
    val state = _state.asStateFlow()
    private val _error = Channel<String>()
    val error = _error.receiveAsFlow()

    fun getUserData() {
        viewModelScope.launch {
            try {
                _state.value = State.Loading
                _results.value = repository.getRandomUser()
                _state.value = State.Finish
            } catch (e: ApiException) {
                _state.value = State.Error
                _error.send(e.message!!)
            } catch (e: Exception) {
                _state.value = State.Error
                _error.send(e.message!!)
            }
        }
    }
}