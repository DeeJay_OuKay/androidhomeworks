package com.example.quiz

import android.view.View
import com.example.quiz.R.array.questions
import com.example.quiz.R.array.authors
import com.example.quiz.R.array.answers0
import com.example.quiz.R.array.answers1
import com.example.quiz.R.array.answers2
import com.example.quiz.R.array.hints0
import com.example.quiz.R.array.hints1
import com.example.quiz.R.array.hints2
import com.example.quiz.R.plurals.correct_answers

object QuizStorage {
    fun getQuiz(view: View): List<Poll> {
        val questions: Array<String> = view.resources.getStringArray(questions)
        val authors: Array<String> = view.resources.getStringArray(authors)
        val answers0: Array<String> = view.resources.getStringArray(answers0)
        val answers1: Array<String> = view.resources.getStringArray(answers1)
        val answers2: Array<String> = view.resources.getStringArray(answers2)
        val hints0: Array<String> = view.resources.getStringArray(hints0)
        val hints1: Array<String> = view.resources.getStringArray(hints1)
        val hints2: Array<String> = view.resources.getStringArray(hints2)
        return listOf(
            Poll(
                question = questions[0],
                author = authors[0],
                answers = answers0.toList(),
                hints = hints0.toList(),
                correct = 0
            ),
            Poll(
                question = questions[1],
                author = authors[1],
                answers = answers1.toList(),
                hints = hints1.toList(),
                correct = 1
            ),
            Poll(
                question = questions[2],
                author = authors[2],
                answers = answers2.toList(),
                hints = hints2.toList(),
                correct = 2
            )
        )
    }

    fun answer(answers: List<String?>, view: View): Pair<String,Boolean>{
        val polls = getQuiz(view)
        var result = ""
        var allCorrect = true
        var correctCount = 0
        for ((i, poll) in polls.withIndex())
        {
            if (answers[i]?.equals(poll.answers[poll.correct]) == true)
            {
                result += "" +
                        "${view.resources.getString(R.string.correct_answer)}:\n${poll.author}: \"${poll.question}\"\n" +
                        "${poll.hints[poll.correct]}: \"${poll.answers[poll.correct]}\"\n"
                correctCount++
            }
            else
            {
                allCorrect = false
                result += "" +
                        "${view.resources.getString(R.string.incorrect_answer)}:\n\"${poll.question}\"\n" +
                        "Вопрос задал ${poll.author}, а ответить должен: ${poll.hints[poll.correct]}\n"
                for ((j, answer) in poll.answers.withIndex())
                {
                    if (answer == answers[j])
                        result += "Ваш ответ кому-то другому \"${answer}\" дал ${poll.hints[j]}"
                }
            }
            result += "\n"
        }
        result += view.resources.getQuantityString(R.plurals.correct_answers, correctCount, correctCount)
        return Pair(result,allCorrect)
    }
}
