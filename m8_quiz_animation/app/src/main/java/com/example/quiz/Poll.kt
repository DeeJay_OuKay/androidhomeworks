package com.example.quiz

class Poll(
    val question:String,
    val author:String,
    val answers:List<String>,
    val hints:List<String>,
    val correct:Int
)
