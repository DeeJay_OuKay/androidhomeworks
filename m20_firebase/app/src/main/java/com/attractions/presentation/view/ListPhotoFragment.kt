package com.attractions.presentation.view

import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.attractions.App
import com.attractions.R
import com.attractions.databinding.FragmentListPhotoBinding
import com.attractions.entity.Photo
import com.attractions.presentation.adapter.ListPhotoAdapter
import com.attractions.presentation.viewmodel.ListPhotoViewModel
import com.attractions.presentation.viewmodel.ListPhotoViewModelFactory
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject
import kotlin.random.Random

private const val FORMAT = "dd.MM.yyyy"

@AndroidEntryPoint
class ListPhotoFragment : Fragment() {
    @Inject
    lateinit var listPhotoViewModelFactory: ListPhotoViewModelFactory
    private val viewModel: ListPhotoViewModel by viewModels { listPhotoViewModelFactory }
    private var _binding: FragmentListPhotoBinding? = null
    private val binding get() = _binding!!
    private val listPhotoAdapter = ListPhotoAdapter(onItemClicked = { photo -> onItemClicked(photo)})

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentListPhotoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            photosRecyclerview.adapter = listPhotoAdapter

            takePhotoButton.setOnClickListener {
                //findNavController().navigate(R.id.action_listPhotoFragment_to_photographFragment)
                FirebaseCrashlytics.getInstance().log("Test Photo Crash ext. info")
                try {
                    throw RuntimeException("Test Photo Crash")
                } catch (e: Exception) {
                    FirebaseCrashlytics.getInstance().recordException(e)
                }
            }

            mapButton.setOnClickListener {
                findNavController().navigate(R.id.action_listPhotoFragment_to_mapFragment)
                //FirebaseCrashlytics.getInstance().log("Test Map Crash ext. info")
                //throw RuntimeException("Test Map Crash")
                createNotification()
            }
            FirebaseMessaging.getInstance().token.addOnCompleteListener {
                Log.d("!token!", it.result)
            }
        }

        viewModel.photos.onEach {
            listPhotoAdapter.submitList(it)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    companion object {
        private const val NOTIFICATION_ID = 1
    }

    private fun createNotification() {
        val intent = Intent(requireContext(),MainActivity::class.java)
        val pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
            PendingIntent.getActivity(requireContext(),0,intent,PendingIntent.FLAG_IMMUTABLE)
        else
            PendingIntent.getActivity(requireContext(),0,intent,PendingIntent.FLAG_UPDATE_CURRENT)
        val notification = NotificationCompat.Builder(requireContext(), App.NOTIFICATION_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_notification)
            .setContentTitle("Тык по кнопке")
            .setContentText("Была нажата кнопка для просмотра карты ${System.currentTimeMillis()}")
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build()
        //NotificationManagerCompat.from(requireContext()).notify(NOTIFICATION_ID,notification)
        NotificationManagerCompat.from(requireContext()).notify(Random.nextInt(),notification)
    }

    private fun onItemClicked(photo: Photo) {
        val bundle = Bundle().apply {
            putString(ARG_URI, photo.uri)
            putString(ARG_DATE, SimpleDateFormat(FORMAT, Locale.getDefault()).format(photo.date.time))
        }
        findNavController().navigate(R.id.action_listPhotoFragment_to_photoFragment, bundle)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}